#include "sqserv.h"
#include <errno.h>
#include <err.h>
#include <sysexits.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <libgen.h>

#undef LEGACY

char *prog_name;

void usage()
{
    printf("usage: %s <port>\n", prog_name);
}

int main(int argc, char* argv[]) {
    long port = DEFAULT_PORT;
    struct sockaddr_in sin;
    int fd;

    debug("debug is on\n");

    if (argc != 2) {
        usage();
        return EX_USAGE;
    }

    port = strtol(argv[1], (char **)NULL, 10);
    printf("Trying to connect to port %d\n", (int)port);
    fd = socket(AF_INET, SOCK_STREAM, 0);


    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY; // rather memcpy to sin.sin_addr
    sin.sin_port = htons(port);

    if (connect(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
        err(EX_OSERR, "connect failed");
    }

    printf("Connected\n");
    /* transfer data*/
    int n = 0;
    int data_len = BUFFER_SIZE;
    char *data = (char *)malloc(data_len);
    while (1==1) {
        printf("Enter the number : ");
        n = 0;
        while ((data[n++] = getchar()) != '\n')
            ;
        send(fd, data, data_len, 0);
        recv(fd, data, data_len, 0);

        printf("From Server : %s\n", data);
    }

    /* cleanup */
    free(data);
    close(fd);

    return 0;
}
