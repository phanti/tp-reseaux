#include "sqserv.h"
#include <errno.h>
#include <err.h>
#include <sysexits.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <libgen.h>

void usage()
{
    printf("usage: -p <port>\n");
}

int main(int argc, char *argv[])
{
    long port = DEFAULT_PORT;
    struct sockaddr_in sin;
    int fd, connectedfd;
    socklen_t sin_len;
    size_t data_len;
    char *data, *end;
    long ch;

    if (argc != 1 && argc != 3)
    {
        usage();
        return EX_USAGE;
    }
    if (argc == 3)
    {
        if (strcmp(argv[1], "-p") == 0)
        {
            port = strtol(argv[2], (char **)NULL, 10);
        }
        else
        {
            usage();
            return EX_USAGE;
        }
    }

    printf("listening on port %d\n", (int)port);

    /* get room for data */
    data_len = BUFFER_SIZE;
    if ((data = (char *)malloc(data_len)) < 0)
    {
        err(EX_SOFTWARE, "in malloc");
    }

    /* create and bind a socket */
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0)
    {
        free(data);
        err(EX_SOFTWARE, "in socket");
    }

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY; // rather memcpy to sin.sin_addr
    sin.sin_port = htons(port);

    if (bind(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        free(data);
        close(fd);
        err(EX_SOFTWARE, "in bind");
    }

    /* listen for connections */
    if (listen(fd, 5) < 0)
    {
        free(data);
        close(fd);
        err(EX_SOFTWARE, "in listen");
    }

    printf("Listening\n\n");
    sin_len = sizeof(sin);

    // accept a connection
    if ((connectedfd = accept(fd, (struct sockaddr *)&sin, &sin_len)) < 0)
    {
        free(data);
        close(fd);
        err(EX_SOFTWARE, "in accept");
    }

    printf("Accepted\n\n");

    /* get room for data */
    data_len = BUFFER_SIZE;
    if ((data = (char *)malloc(data_len)) < 0)
    {
        err(EX_SOFTWARE, "in malloc");
    }

    /* data transfer */
    while (1 == 1)
    {

        recv(connectedfd, data, data_len, 0);
        ch = strtol(data, &end, 10);

        switch (errno)
        {
        case EINVAL:
            err(EX_DATAERR, "not an integer");
        case ERANGE:
            err(EX_DATAERR, "out of range");
        default:
            if (ch == 0 && data == end)
            {
                errx(EX_DATAERR, "no value");
            }
        }

        printf("integer value: %ld\n", ch);
        sprintf(data, "%ld", ch * ch);

        // and send that buffer to client
        send(connectedfd, data, data_len, 0);
    }
    /* cleanup */
    free(data);
    close(fd);
    close(connectedfd);
    return EX_OK;
}
