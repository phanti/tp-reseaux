# GNU Makefile
# using implicit rules
# generate targets for every .c file (only standalone .c commands)

CFLAGS+=-Wall
CC=gcc

sources = \
Makefile \
sqUDPclient.c \
sqUDPserver.c \
sqTCPserverSel.c \
sqTCPclient.c \
sqTCPserver.c \
sqserv.h \
SqTCPServ.java

# generate targets, only standalone .c file
execs = $(filter %.c,$(sources))
classes = $(filter %.java,$(sources))
targets = $(execs:.c=) $(classes:.java=.class)

all: $(targets)

SqTCPServ.class: SqTCPServ.java
	javac SqTCPServ.java

.PHONY : clean

clean:
	$(RM) $(targets)
