#define DEFAULT_PORT 5000
#define BUFFER_SIZE 128

#ifdef DEBUG
#include <stdio.h>
#define debug(fmt, ...) printf(fmt, ##__VA_ARGS__)
#else
#define debug(fmt, ...)
#endif
