#include "sqserv.h"
#include <errno.h>
#include <err.h>
#include <sysexits.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <libgen.h>
#include <sys/time.h>

#define MAX_CONNECTION 10
#ifndef FD_COPY
#define FD_COPY(orig, dest) memcpy((dest), (orig), sizeof(*(dest)))
#endif

void usage()
{
    printf("usage: -p <port>\n");
}

int main(int argc, char *argv[])
{
    long port = DEFAULT_PORT;
    struct sockaddr_in sin;
    int fd, connectedfd;
    socklen_t sin_len;
    size_t data_len;
    char *data, *end;
    long ch;

    if (argc != 1 && argc != 3)
    {
        usage();
        return EX_USAGE;
    }
    if (argc == 3)
    {
        if (strcmp(argv[1], "-p") == 0)
        {
            port = strtol(argv[2], (char **)NULL, 10);
        }
        else
        {
            usage();
            return EX_USAGE;
        }
    }

    printf("listening on port %d\n", (int)port);

    /* get room for data */
    data_len = BUFFER_SIZE;
    if ((data = (char *)malloc(data_len)) < 0)
    {
        err(EX_SOFTWARE, "in malloc");
    }

    /* create and bind a socket */
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0)
    {
        free(data);
        err(EX_SOFTWARE, "in socket");
    }

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY; // rather memcpy to sin.sin_addr
    sin.sin_port = htons(port);

    if (bind(fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        free(data);
        close(fd);
        err(EX_SOFTWARE, "in bind");
    }

    /* listen for connections */
    if (listen(fd, 5) < 0)
    {
        free(data);
        close(fd);
        err(EX_SOFTWARE, "in listen");
    }

    printf("Listening\n\n");
    sin_len = sizeof(sin);

    fd_set rd_mask, wr_mask, ex_mask, rd_select, wr_select, ex_select;
    int max_fd = fd + 1;
    int i, nb_set;
    int all_connections[MAX_CONNECTION];
    FD_ZERO(&rd_mask);
    FD_ZERO(&wr_mask);
    FD_ZERO(&ex_mask);
    FD_SET(fd, &rd_mask);
    FD_SET(fd, &ex_mask);


    // init all connections to -1
    for (size_t i = 0; i < MAX_CONNECTION; i++)
    {
        all_connections[i] = -1;
    }

    while (1)
    {
        // select will modify the fd_set, so we need to copy it
        FD_COPY(&rd_mask, &rd_select);
        FD_COPY(&wr_mask, &wr_select);
        FD_COPY(&ex_mask, &ex_select);

        // select call
        nb_set = select(max_fd, &rd_select, &wr_select, &ex_select, NULL);

        // check if select failed
        if (nb_set == -1)
        {
            perror("select");
            exit(1);
        }

        // there are events
        if (nb_set > 0)
        {
            // first check if there is a new connection
            if (FD_ISSET(fd, &rd_select))
            {
                // accept the connection
                if ((connectedfd = accept(fd, (struct sockaddr *)&sin, &sin_len)) < 0)
                {
                    free(data);
                    close(fd);
                    err(EX_SOFTWARE, "in accept");
                }
                // add the new connection to the list
                for (i = 0; i < MAX_CONNECTION; i++)
                {
                    if (all_connections[i] < 0)
                    {
                        all_connections[i] = connectedfd;
                        break;
                    }
                }

                printf("Accepted a new connection with fd: %d\n", connectedfd);
                max_fd += 1;


                FD_SET(connectedfd, &rd_mask);
                FD_SET(connectedfd, &ex_mask);
                FD_SET(connectedfd, &wr_mask);
            }

            // check if there is an event on an existing connection
            for (size_t i = 0; i < MAX_CONNECTION; i++)
            {
                // if the connection is active and there is an event on it
                if (all_connections[i] > 0 && FD_ISSET(all_connections[i], &rd_select))
                {
                    // read the data
                    recv(all_connections[i], data, data_len, 0);
                    ch = strtol(data, &end, 10);
                    switch (errno)
                    {
                    case EINVAL:
                        err(EX_DATAERR, "not an integer");
                    case ERANGE:
                        err(EX_DATAERR, "out of range");
                    default:
                        if (ch == 0 && data == end)
                        {
                            errx(EX_DATAERR, "no value");
                        }
                    }

                    printf("integer value: %ld\n", ch);
                    sprintf(data, "%ld", ch * ch);

                    // and send buffer to client
                    send(all_connections[i], data, data_len, 0);
                }
            }
        }
    }

    free(data);
    close(fd);
    // close all connections
    for (size_t i = 0; i < MAX_CONNECTION; i++)
    {
        if (all_connections[i] > 0)
        {
            close(all_connections[i]);
        }
    }
}
